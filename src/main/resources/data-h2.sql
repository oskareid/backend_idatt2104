INSERT INTO UserAccount (user_id, first_name, surname, email, salt) VALUES ( 32, 'Kristian Valset', 'Aars', 'krisvaa@stud.ntnu.no', SECURE_RAND(64));
INSERT INTO UserAccount (user_id, first_name, surname, email, salt) VALUES ( 33, 'Oskar langås', 'Eidem', 'oskareid@stud.ntnu.no', SECURE_RAND(64) );
INSERT INTO Student (user_id) VALUES ( 32 );
INSERT INTO Student (user_id) VALUES ( 33 );

INSERT INTO UserAccount (user_id, first_name, surname, email, salt) VALUES ( 34, 'Tomas', 'Holt', 'tomas.holtt@ntnu.no', SECURE_RAND(64) );
INSERT INTO UserAccount (user_id, first_name, surname, email, salt) VALUES ( 35, 'Ole Christian', 'Eidheim', 'ole.c.eidheimm@ntnu.no',SECURE_RAND(64) );
INSERT INTO Teacher (user_id) VALUES ( 34 );
INSERT INTO Teacher (user_id) VALUES ( 35 );

INSERT INTO UserAccount (user_id, first_name, surname, email, salt) VALUES ( 36, 'Lea', 'Grønning', 'leagggg@stud.ntnu.no',  SECURE_RAND(64));
INSERT INTO UserAccount (user_id, first_name, surname, email, salt) VALUES ( 37, 'Carl', 'Labrador', 'carl.lab@stud.ntnu.no', SECURE_RAND(64));
INSERT INTO Student VALUES ( 36 );
INSERT INTO StudentAssistant VALUES ( 36 );
INSERT INTO Student VALUES ( 37 );
INSERT INTO StudentAssistant VALUES ( 37 );

UPDATE USERACCOUNT SET PASSWORD = HASH('SHA-512', SALT || 'password', 1000);

INSERT INTO SubjectInfo (subject_code, name) VALUES ( 'IDATT2105' , 'Fullstack Applikasjonsutvikling');
INSERT INTO SubjectInfo (subject_code, name) VALUES ( 'IDATT2104' , 'Nettverksprogrammering');

INSERT INTO Subject (subject_code, semester) VALUES ( 'IDATT2105', 'V21' );
INSERT INTO SubjectQueue (subject_code, semester, active, archived) VALUES ( 'IDATT2105', 'V21', false, true );

INSERT INTO Subject (subject_code, semester) VALUES ( 'IDATT2105', 'V22' );
INSERT INTO SubjectQueue (subject_code, semester, active) VALUES ( 'IDATT2105', 'V22', true );

INSERT INTO Subject (subject_code, semester) VALUES ( 'IDATT2104', 'V21' );
INSERT INTO SubjectQueue (subject_code, semester, archived) VALUES ( 'IDATT2104', 'V21', true );

INSERT INTO Subject (subject_code, semester) VALUES ( 'IDATT2104', 'V22' );
INSERT INTO SubjectQueue (subject_code, semester, active) VALUES ( 'IDATT2104', 'V22', false );

INSERT INTO Teacher_Subject  (subject_code, semester, teacher_user_id) VALUES ( 'IDATT2105', 'V21', 34 );
INSERT INTO Teacher_Subject  (subject_code, semester, teacher_user_id) VALUES ( 'IDATT2105', 'V22', 34 );
INSERT INTO Teacher_Subject  (subject_code, semester, teacher_user_id) VALUES ( 'IDATT2104', 'V21', 35 );
INSERT INTO Teacher_Subject  (subject_code, semester, teacher_user_id) VALUES ( 'IDATT2104', 'V22', 35 );

INSERT INTO StudentAssistant_Subject (subject_code, semester, student_assistant_user_id) VALUES ( 'IDATT2105', 'V22', 36 );
INSERT INTO StudentAssistant_Subject (subject_code, semester, student_assistant_user_id) VALUES ( 'IDATT2105', 'V22', 37 );

INSERT INTO StudentAssistant_Subject (subject_code, semester, student_assistant_user_id) VALUES ( 'IDATT2104', 'V22', 37 );

INSERT INTO Student_Subject (subject_code, semester, student_user_id) VALUES ( 'IDATT2105', 'V22', 32);
INSERT INTO Student_Subject (subject_code, semester, student_user_id) VALUES ( 'IDATT2105', 'V22', 33);

INSERT INTO Student_Subject (subject_code, semester, student_user_id) VALUES ( 'IDATT2104', 'V22', 32);
INSERT INTO Student_Subject (subject_code, semester, student_user_id) VALUES ( 'IDATT2104', 'V22', 33);

INSERT INTO Assignment (subject_code, semester, assignment_number) VALUES ( 'IDATT2105', 'V22', 1 );
INSERT INTO Assignment (subject_code, semester, assignment_number) VALUES ( 'IDATT2105', 'V22', 2 );
INSERT INTO Assignment (subject_code, semester, assignment_number) VALUES ( 'IDATT2105', 'V22', 3 );

INSERT INTO Assignment (subject_code, semester, assignment_number) VALUES ( 'IDATT2104', 'V22', 3 );
INSERT INTO Assignment (subject_code, semester, assignment_number) VALUES ( 'IDATT2105', 'V21', 4 );
INSERT INTO Assignment (subject_code, semester, assignment_number) VALUES ( 'IDATT2104', 'V21', 5 );

INSERT INTO AssignmentStatus (subject_code, semester, assignment_number, student_user_id, approved, approved_time) VALUES ('IDATT2105', 'V22', 1, 32, true, CURRENT_TIMESTAMP());
INSERT INTO AssignmentStatus (subject_code, semester, assignment_number, student_user_id) VALUES ('IDATT2105', 'V22', 2, 32);
INSERT INTO AssignmentStatus (subject_code, semester, assignment_number, student_user_id) VALUES ('IDATT2105', 'V22', 3, 32);

INSERT INTO AssignmentStatus (subject_code, semester, assignment_number, student_user_id, approved, approved_time) VALUES ('IDATT2105', 'V22', 1, 33, true, CURRENT_TIMESTAMP());
INSERT INTO AssignmentStatus (subject_code, semester, assignment_number, student_user_id) VALUES ('IDATT2105', 'V22', 2, 33);
INSERT INTO AssignmentStatus (subject_code, semester, assignment_number, student_user_id) VALUES ('IDATT2105', 'V22', 3, 33);

