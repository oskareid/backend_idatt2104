CREATE TABLE UserAccount  (
    user_id INTEGER AUTO_INCREMENT NOT NULL,
    first_name VARCHAR(75) NOT NULL,
    surname VARCHAR(75) NOT NULL,
    email VARCHAR(75) NOT NULL,
    salt VARBINARY(64) NOT NULL,
    password VARBINARY(64),
    PRIMARY KEY (user_id)
);

CREATE TABLE Student (
    user_id INTEGER NOT NULL,
    PRIMARY KEY (user_id)
);

CREATE TABLE StudentAssistant (
    user_id INTEGER NOT NULL,
    PRIMARY KEY (user_id)
);

CREATE TABLE Teacher (
     user_id INTEGER NOT NULL,
     PRIMARY KEY (user_id)
);

CREATE TABLE SubjectInfo (
    subject_code VARCHAR(12) NOT NULL,
    name VARCHAR(75) NOT NULL,
    PRIMARY KEY (subject_code)
);

CREATE TABLE Subject (
     subject_code VARCHAR(12) NOT NULL,
     semester VARCHAR(3) NOT NULL,
     PRIMARY KEY (subject_code, semester)
);

CREATE TABLE Teacher_Subject(
    subject_code VARCHAR(12) NOT NULL,
    semester VARCHAR(3) NOT NULL,
    teacher_user_id INTEGER NOT NULL,
    PRIMARY KEY (subject_code, semester, teacher_user_id)
);

CREATE TABLE StudentAssistant_Subject(
    subject_code VARCHAR(12) NOT NULL,
    semester VARCHAR(3) NOT NULL,
    student_assistant_user_id INTEGER NOT NULL,
    PRIMARY KEY (subject_code, semester, student_assistant_user_id)
);

CREATE TABLE Student_Subject(
    subject_code VARCHAR(12) NOT NULL,
    semester VARCHAR(3) NOT NULL,
    student_user_id INTEGER NOT NULL,
    PRIMARY KEY (subject_code, semester, student_user_id)
);

CREATE TABLE SubjectQueue (
    subject_code VARCHAR(12) NOT NULL,
    semester VARCHAR(3) NOT NULL,
    active BOOLEAN NOT NULL DEFAULT false,
    archived BOOLEAN NOT NULL DEFAULT false,
    PRIMARY KEY (subject_code, semester)
);

CREATE TABLE SubjectQueuePosition(
    subject_code VARCHAR(12) NOT NULL,
    semester VARCHAR(3) NOT NULL,
    assignment_number INTEGER NOT NULL,
    student_user_id INTEGER NOT NULL,
    entry_time DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    queue_type VARCHAR(12) NOT NULL,
    status VARCHAR(12),
    active_student_assistant_id INTEGER DEFAULT -1,
    PRIMARY KEY (subject_code, semester, student_user_id)
);

CREATE TABLE Assignment (
    subject_code VARCHAR(12) NOT NULL,
    semester VARCHAR(3) NOT NULL,
    assignment_number INTEGER NOT NULL,
    PRIMARY KEY (subject_code, semester, assignment_number)
);

CREATE TABLE AssignmentStatus (
    subject_code VARCHAR(12) NOT NULL,
    semester VARCHAR(3) NOT NULL,
    assignment_number INTEGER NOT NULL,
    student_user_id INTEGER NOT NULL,
    approved BOOLEAN NOT NULL DEFAULT false,
    approved_time DATETIME DEFAULT null,
    PRIMARY KEY (subject_code, semester, assignment_number, student_user_id)
);

// Configure dependencies (FK/PK)
ALTER TABLE Student
    ADD CONSTRAINT FK_Student_user_id
    FOREIGN KEY (user_id)
    REFERENCES UserAccount (user_id);

ALTER TABLE Teacher
    ADD CONSTRAINT FK_Teacher_user_id
    FOREIGN KEY (user_id)
    REFERENCES UserAccount (user_id);

ALTER TABLE StudentAssistant
    ADD CONSTRAINT FK_StudentAssistant_user_id
    FOREIGN KEY (user_id)
    REFERENCES UserAccount (user_id);

ALTER TABLE Subject
    ADD CONSTRAINT FK_Subject_subject_code
    FOREIGN KEY (subject_code)
    REFERENCES SubjectInfo(subject_code);

ALTER TABLE SubjectQueue
    ADD CONSTRAINT FK_SubjectQueue_subject
    FOREIGN KEY (subject_code, semester)
    REFERENCES Subject(subject_code, semester);

ALTER TABLE SubjectQueuePosition
    ADD CONSTRAINT FK_SubjectQueuePosition_sub_queue
    FOREIGN KEY (subject_code, semester)
    REFERENCES SubjectQueue(subject_code, semester);

ALTER TABLE SubjectQueuePosition
    ADD CONSTRAINT FK_SubjectQueuePosition_Assignment
    FOREIGN KEY (subject_code, semester, assignment_number)
    REFERENCES Assignment(subject_code, semester, assignment_number);

ALTER TABLE Teacher_Subject
    ADD CONSTRAINT FK_Teacher_Subject_teacher_user_id
    FOREIGN KEY (teacher_user_id)
    REFERENCES UserAccount(user_id);

ALTER TABLE Teacher_Subject
    ADD CONSTRAINT FK_Teacher_Subject_subject
    FOREIGN KEY (subject_code, semester)
    REFERENCES Subject(subject_code, semester);

ALTER TABLE Student_Subject
    ADD CONSTRAINT FK_Student_Subject_student_user_id
    FOREIGN KEY (student_user_id)
    REFERENCES UserAccount(user_id);

ALTER TABLE Student_Subject
    ADD CONSTRAINT FK_Student_Subject_subject
    FOREIGN KEY (subject_code, semester)
    REFERENCES Subject(subject_code, semester);

ALTER TABLE StudentAssistant_Subject
    ADD CONSTRAINT FK_StudentAssistant_Subject_teacher_user_id
    FOREIGN KEY (student_assistant_user_id)
    REFERENCES UserAccount(user_id);

ALTER TABLE StudentAssistant_Subject
    ADD CONSTRAINT FK_StudentAssistant_Subject_subject
    FOREIGN KEY (subject_code, semester)
    REFERENCES Subject(subject_code, semester);
