package no.ntnu.idatt2105.qs3.QS3Backend.model.key;

import java.io.Serializable;
import java.util.Objects;

/**
 * Primary key structure for {@link no.ntnu.idatt2105.qs3.QS3Backend.model.SubjectQueuePosition}
 */
public class SubjectQueuePositionKey implements Serializable {

    private String subject_code;
    private String semester;
    private int student_user_id;

    public SubjectQueuePositionKey() {
    }

    public SubjectQueuePositionKey(String subject_code, String semester, int student_user_id) {
        this.subject_code = subject_code;
        this.semester = semester;
        this.student_user_id = student_user_id;
    }

    public String getSubject_code() {
        return subject_code;
    }

    public void setSubject_code(String subject_code) {
        this.subject_code = subject_code;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public int getStudent_user_id() {
        return student_user_id;
    }

    public void setStudent_user_id(int student_user_id) {
        this.student_user_id = student_user_id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SubjectQueuePositionKey that = (SubjectQueuePositionKey) o;

        if (student_user_id != that.student_user_id) return false;
        if (!Objects.equals(subject_code, that.subject_code)) return false;
        return Objects.equals(semester, that.semester);
    }

    @Override
    public int hashCode() {
        int result = subject_code != null ? subject_code.hashCode() : 0;
        result = 31 * result + (semester != null ? semester.hashCode() : 0);
        result = 31 * result + student_user_id;
        return result;
    }
}
