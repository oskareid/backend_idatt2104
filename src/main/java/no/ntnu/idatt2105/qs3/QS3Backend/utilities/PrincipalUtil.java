package no.ntnu.idatt2105.qs3.QS3Backend.utilities;

import java.security.Principal;

public class PrincipalUtil {

    public static int getUserId(Principal p) {
        return Integer.parseInt(p.getName());
    }

}
