package no.ntnu.idatt2105.qs3.QS3Backend.repository;

import no.ntnu.idatt2105.qs3.QS3Backend.model.AssignmentStatus;
import no.ntnu.idatt2105.qs3.QS3Backend.model.SubjectQueuePosition;
import no.ntnu.idatt2105.qs3.QS3Backend.model.key.AssignmentStatusKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AssignmentStatusRepository extends JpaRepository<AssignmentStatus, AssignmentStatusKey> {

    @Query("SELECT asgs FROM AssignmentStatus asgs WHERE asgs.subject_code = :subject_code AND asgs.semester = :semester AND asgs.student_user_id = :student_user_id")
    List<AssignmentStatus> findBySubjectKey(
            @Param("subject_code") String subject_code,
            @Param("semester") String semester,
            @Param("student_user_id") int student_user_id
    );

}
