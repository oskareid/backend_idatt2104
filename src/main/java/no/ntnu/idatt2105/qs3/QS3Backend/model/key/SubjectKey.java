package no.ntnu.idatt2105.qs3.QS3Backend.model.key;

import java.io.Serializable;
import java.util.Objects;

/**
 * Primary Key structure for {@link no.ntnu.idatt2105.qs3.QS3Backend.model.Subject}
 */
public class SubjectKey implements Serializable {

    private String subject_code;
    private String semester;

    public SubjectKey() {}

    public SubjectKey(String subject_code, String semester) {
        this.subject_code = subject_code;
        this.semester = semester;
    }

    public String getSubject_code() {
        return subject_code;
    }

    public void setSubject_code(String subject_code) {
        this.subject_code = subject_code;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SubjectKey that = (SubjectKey) o;

        if (!Objects.equals(subject_code, that.subject_code)) return false;
        return Objects.equals(semester, that.semester);
    }

    @Override
    public int hashCode() {
        int result = subject_code != null ? subject_code.hashCode() : 0;
        result = 31 * result + (semester != null ? semester.hashCode() : 0);
        return result;
    }
}
