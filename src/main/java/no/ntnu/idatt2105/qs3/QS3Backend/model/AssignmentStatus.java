package no.ntnu.idatt2105.qs3.QS3Backend.model;

import no.ntnu.idatt2105.qs3.QS3Backend.model.key.AssignmentStatusKey;

import javax.persistence.*;

@Entity
@IdClass(AssignmentStatusKey.class)
@Table(name = "ASSIGNMENTSTATUS")
public class AssignmentStatus {

    @Id
    private String subject_code;
    @Id
    private String semester;
    @Id
    private int assignment_number;
    @Id
    private int student_user_id;

    @ManyToOne
    private Assignment assignment;

    private boolean approved;

    public AssignmentStatus() {
    }

    public Assignment getAssignment() {
        return assignment;
    }

    public void setAssignment(Assignment assignment) {
        this.assignment = assignment;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public String getSubject_code() {
        return subject_code;
    }

    public void setSubject_code(String subject_code) {
        this.subject_code = subject_code;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public int getStudent_user_id() {
        return student_user_id;
    }

    public void setStudent_user_id(int student_user_id) {
        this.student_user_id = student_user_id;
    }

    public int getAssignment_number() {
        return assignment_number;
    }
}
