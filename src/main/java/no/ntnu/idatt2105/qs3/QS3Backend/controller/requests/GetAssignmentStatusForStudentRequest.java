package no.ntnu.idatt2105.qs3.QS3Backend.controller.requests;

public class GetAssignmentStatusForStudentRequest {

    private String subject_code;
    private String semester;
    private int student_user_id;

    public GetAssignmentStatusForStudentRequest() {
    }

    public String getSubject_code() {
        return subject_code;
    }

    public String getSemester() {
        return semester;
    }

    public int getStudent_user_id() {
        return student_user_id;
    }

}
