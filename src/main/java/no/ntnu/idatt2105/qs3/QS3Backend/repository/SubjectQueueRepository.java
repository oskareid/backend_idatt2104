package no.ntnu.idatt2105.qs3.QS3Backend.repository;

import no.ntnu.idatt2105.qs3.QS3Backend.model.SubjectQueue;
import no.ntnu.idatt2105.qs3.QS3Backend.model.key.SubjectKey;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubjectQueueRepository extends CrudRepository<SubjectQueue, SubjectKey> {

    @Override
    Iterable<SubjectQueue> findAll();

}
