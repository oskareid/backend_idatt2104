package no.ntnu.idatt2105.qs3.QS3Backend.controller.requests;

public class ChangeQueueStatusRequest {
    //TODO: Javadoc
    private boolean active;

    public ChangeQueueStatusRequest() {
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
