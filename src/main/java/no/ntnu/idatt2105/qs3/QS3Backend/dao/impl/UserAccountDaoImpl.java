package no.ntnu.idatt2105.qs3.QS3Backend.dao.impl;

import no.ntnu.idatt2105.qs3.QS3Backend.dao.UserAccountDao;
import no.ntnu.idatt2105.qs3.QS3Backend.exception.UnknownCredentialsException;
import no.ntnu.idatt2105.qs3.QS3Backend.model.UserAccount;
import no.ntnu.idatt2105.qs3.QS3Backend.security.SecurityConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Repository
public class UserAccountDaoImpl implements UserAccountDao {

    private final Logger logger = Logger.getLogger("UserAccountDaoImpl");

    @Autowired
    private JdbcTemplate jdbcTemplate;


    public UserAccountDaoImpl() {}

    @Override
    public UserAccount authenticateUser(String email, String password) throws UnknownCredentialsException {
        try {
            final UserAccount result = jdbcTemplate.queryForObject(
                    "SELECT * FROM UserAccount usr " +
                            "WHERE usr.email = ?" +
                            "AND usr.password = HASH('SHA-512', salt || ?, 1000)",
                    BeanPropertyRowMapper.newInstance(UserAccount.class),
                    email, password
            );

            return result;
        } catch (IncorrectResultSizeDataAccessException e) {
            throw new UnknownCredentialsException("Unknown username/password for " + email);
        }
    }

    @SuppressWarnings("ConstantConditions")
    public List<String> getRolesForUserId(int userId) {
        final int Student = jdbcTemplate.queryForObject(
                "SELECT COUNT(*) FROM Student " +
                        "WHERE Student.user_id = ?",
                Integer.class,
                userId
        );

        final int StudentAssistant = jdbcTemplate.queryForObject(
                "SELECT COUNT(*) FROM StudentAssistant " +
                        "WHERE StudentAssistant.user_id = ?",
                Integer.class,
                userId
        );

        final int Teacher = jdbcTemplate.queryForObject(
                "SELECT COUNT(*) FROM Teacher " +
                        "WHERE Teacher.user_id = ?",
                Integer.class,
                userId
        );

        List<String> roles = new ArrayList<>();
        if(Student==1) {roles.add(SecurityConstants.ROLE_STUDENT);}
        if(StudentAssistant==1) {roles.add(SecurityConstants.ROLE_STUDENT_ASSISTANT);}
        if(Teacher==1) {roles.add(SecurityConstants.ROLE_TEACHER);}

        return roles;
    }

    @Override
    public UserAccount readFromEmail(String email) {
        try {
            return jdbcTemplate.queryForObject(
                    "SELECT * FROM UserAccount usr " +
                            "WHERE usr.email = ?",
                    BeanPropertyRowMapper.newInstance(UserAccount.class),
                    email
            );
        } catch (IncorrectResultSizeDataAccessException e) {
            logger.warning("Unexpected result-size on set when attempting to retrieve user " + email);
            throw e;
        }
    }

    @Override
    public UserAccount readFromId(int userId) {
        try {
            return jdbcTemplate.queryForObject(
                    "SELECT first_name, surname, email FROM UserAccount usr " +
                            "WHERE usr.user_id = ?",
                    BeanPropertyRowMapper.newInstance(UserAccount.class),
                    userId
            );
        } catch (IncorrectResultSizeDataAccessException e) {
            logger.warning("Unexpected result-size on set when attempting to retrieve user " + userId);
            throw e;
        }
    }
}
