package no.ntnu.idatt2105.qs3.QS3Backend.exception;

public class UnknownCredentialsException extends Exception {

    public UnknownCredentialsException(String message) {
        super(message);
    }
}
