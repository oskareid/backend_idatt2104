package no.ntnu.idatt2105.qs3.QS3Backend.model;

import no.ntnu.idatt2105.qs3.QS3Backend.model.key.SubjectQueuePositionKey;

import javax.persistence.*;
import java.util.Date;

/**
 *
 * Queue positions enable students to stand in queue of a given subject. The lifespan of a queue position is as following:
 * - Create new postions when entering queue
 * - Status can be changed to postponed by student-assistant or studen, which will remove student from queue temprary
 * - When Queue-position is completes (Either approved assignment, or completed help-session) the Queue-Position-object will be removed from the database.
 *
 */
@Entity
@IdClass(SubjectQueuePositionKey.class)
@Table(name="SUBJECTQUEUEPOSITION")
public class SubjectQueuePosition {

    /**
     * Subject Code to identify which queue the position is in
     */
    @Id
    @Column( name="subject_code", updatable = false)
    private String subject_code;

    /**
     * Semester to associate the Queue Position with
     */
    @Id
    @Column( name="semester", updatable = false)
    private String semester;

    /**
     * Id of the student of which the position belongs to
     */
    @Id
    @Column( name="student_user_id", updatable = false)
    private int student_user_id;

    /**
     * Id of the student assistant currently helping with this queue position
     */
    private int active_student_assistant_id;

    /**
     * Entry-time in the queue
     */
    @Column(insertable = false, updatable = false)
    private Date entry_time;

    /**
     * Type of assistant the student wishes (Either "Help" or "Approval")
     */
    //TODO: Change to enum?
    @Column( updatable = false)
    private String queue_type;

    /**
     * Status of the queue-position ("Waiting", "In session", or "Postponed")
     */
    //TODO: Change to enum?
    @Column(insertable = false)
    private String status;

    /**
     * Assignment number associated with the queue position. Must be -1 if no assignment shall be associated with the position.
     */
    @Column(name = "assignment_number", updatable = false)
    private int assignment_number;

    /**
     * Student of which the queue position belongs to
     */
    @ManyToOne
    @JoinColumn(name="student_user_id", updatable = false, insertable = false)
    private Student student;

    public SubjectQueuePosition() {
    }

    public SubjectQueuePosition(String subject_code, String semester, int student_user_id, Date entry_time, String queue_type, String status, int assignment_number) {
        this.subject_code = subject_code;
        this.semester = semester;
        this.student_user_id = student_user_id;
        this.entry_time = entry_time;
        this.queue_type = queue_type;
        this.status = status;
        this.assignment_number = assignment_number;
    }

    public String getSubject_code() {
        return subject_code;
    }

    public void setSubject_code(String subject_code) {
        this.subject_code = subject_code;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public void setStudent_user_id(int student_user_id) {
        this.student_user_id = student_user_id;
    }

    public Date getEntry_time() {
        return entry_time;
    }

    public void setEntry_time(Date entry_time) {
        this.entry_time = entry_time;
    }

    public String getQueue_type() {
        return queue_type;
    }

    public void setQueue_type(String queue_type) {
        this.queue_type = queue_type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Student getStudent() {
        return student;
    }

    public int getAssignment_number() {
        return assignment_number;
    }

    public void setAssignment_number(int assignment_number) {
        this.assignment_number = assignment_number;
    }

    public int getStudent_user_id() {
        return student_user_id;
    }

    public int getActive_student_assistant_id() {
        return active_student_assistant_id;
    }

    public void setActive_student_assistant_id(int active_student_assistant_id) {
        this.active_student_assistant_id = active_student_assistant_id;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
}
