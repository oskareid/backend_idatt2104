package no.ntnu.idatt2105.qs3.QS3Backend.model;

import no.ntnu.idatt2105.qs3.QS3Backend.model.key.AssignmentKey;

import javax.persistence.*;

/**
 * Assignment describes a single assignment in a given subject
 */
@Entity
@IdClass(AssignmentKey.class)
@Table(name = "Assignment")
public class Assignment {

    /**
     * Subject Code of the associated subject
     */
    @Id
    @Column(name = "subject_code", updatable = false)
    private String subject_code;

    /**
     * Semester of the associated subject
     */
    @Id
    @Column(name = "semester", updatable = false)
    private String semester;

    /**
     * Assignment number. If value is -1, then assignment is a nil-reference dummy assignment.
     */
    @Id
    @Column(name = "assignment_number", updatable = false)
    private int assignment_number;

    public Assignment() {
    }

    public String getSubject_code() {
        return subject_code;
    }

    public void setSubject_code(String subject_code) {
        this.subject_code = subject_code;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public int getAssignment_number() {
        return assignment_number;
    }

    public void setAssignment_number(int assignment_number) {
        this.assignment_number = assignment_number;
    }
}
