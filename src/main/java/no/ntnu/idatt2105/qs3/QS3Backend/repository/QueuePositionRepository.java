package no.ntnu.idatt2105.qs3.QS3Backend.repository;

import no.ntnu.idatt2105.qs3.QS3Backend.model.SubjectQueuePosition;
import no.ntnu.idatt2105.qs3.QS3Backend.model.key.SubjectQueuePositionKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface QueuePositionRepository extends JpaRepository<SubjectQueuePosition, SubjectQueuePositionKey> {

    @Query("SELECT sqp FROM SubjectQueuePosition sqp WHERE sqp.subject_code = :subject_code AND sqp.semester = :semester")
    List<SubjectQueuePosition> findBySubjectKey(
            @Param("subject_code") String subject_code,
            @Param("semester") String semester
    );

    @Override
    public SubjectQueuePosition save(SubjectQueuePosition entity);

    @Override
    void deleteById(SubjectQueuePositionKey subjectQueuePositionKey);

}
