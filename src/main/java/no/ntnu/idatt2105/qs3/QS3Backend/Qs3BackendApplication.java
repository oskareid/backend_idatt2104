package no.ntnu.idatt2105.qs3.QS3Backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class Qs3BackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(Qs3BackendApplication.class, args);
	}

}
