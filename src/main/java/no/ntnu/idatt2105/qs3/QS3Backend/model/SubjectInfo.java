package no.ntnu.idatt2105.qs3.QS3Backend.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

/**
 * Describes the information of a reoccurring subject.
 *
 */
@Entity
@Table(name = "SUBJECTINFO")
public class SubjectInfo implements Serializable {

    /**
     * Unique subject code.
     *
     * E.g: IDATT2105
     */
    @Id
    private String subject_code;

    /**
     * Display name of the subject.
     */
    private String name;

    public SubjectInfo() {
    }

    public SubjectInfo(String subject_code, String name) {
        this.subject_code = subject_code;
        this.name = name;
    }

    public String getSubject_code() {
        return subject_code;
    }

    public void setSubject_code(String subject_code) {
        this.subject_code = subject_code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "SubjectInfo{" +
                "subject_code='" + subject_code + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SubjectInfo that = (SubjectInfo) o;

        if (!Objects.equals(subject_code, that.subject_code)) return false;
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        int result = subject_code != null ? subject_code.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
