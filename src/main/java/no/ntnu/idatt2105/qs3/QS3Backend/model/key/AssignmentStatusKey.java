package no.ntnu.idatt2105.qs3.QS3Backend.model.key;

import java.io.Serializable;
import java.util.Objects;

public class AssignmentStatusKey implements Serializable {

    private String subject_code;
    private String semester;
    private int assignment_number;
    private int student_user_id;

    public AssignmentStatusKey() {
    }

    public AssignmentStatusKey(String subject_code, String semester, int assignment_number, int student_user_id) {
        this.subject_code = subject_code;
        this.semester = semester;
        this.assignment_number = assignment_number;
        this.student_user_id = student_user_id;
    }

    public String getSubject_code() {
        return subject_code;
    }

    public String getSemester() {
        return semester;
    }

    public int getAssignment_number() {
        return assignment_number;
    }

    public int getStudent_user_id() {
        return student_user_id;
    }

    public void setSubject_code(String subject_code) {
        this.subject_code = subject_code;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public void setAssignment_number(int assignment_number) {
        this.assignment_number = assignment_number;
    }

    public void setStudent_user_id(int student_user_id) {
        this.student_user_id = student_user_id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AssignmentStatusKey that = (AssignmentStatusKey) o;

        if (assignment_number != that.assignment_number) return false;
        if (student_user_id != that.student_user_id) return false;
        if (!Objects.equals(subject_code, that.subject_code)) return false;
        return Objects.equals(semester, that.semester);
    }

    @Override
    public int hashCode() {
        int result = subject_code != null ? subject_code.hashCode() : 0;
        result = 31 * result + (semester != null ? semester.hashCode() : 0);
        result = 31 * result + assignment_number;
        result = 31 * result + student_user_id;
        return result;
    }
}
