package no.ntnu.idatt2105.qs3.QS3Backend.service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import no.ntnu.idatt2105.qs3.QS3Backend.security.SecurityConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class TokenService {

    private final String TOKEN_KEY;
    private final int TOKEN_LIFESPAN;

    @Autowired
    public TokenService() {
        TOKEN_KEY = SecurityConstants.TOKEN_KEY;
        TOKEN_LIFESPAN = SecurityConstants.DEFAULT_TOKEN_LIFESPAN;
    }

    /**
     * Generates a new token for the provided userId.
     *
     * @param userId User id to be added to token.
     * @return Token-string with user id.
     */
    @Transactional(isolation = Isolation.READ_COMMITTED, readOnly = true)
    public String generateToken(final int userId, final List<String> roles) {
        Key key = Keys.hmacShaKeyFor(TOKEN_KEY.getBytes(StandardCharsets.UTF_8));
        List<GrantedAuthority> grantedAuthorities = new ArrayList<>(roles.size());
        roles.forEach(r -> { grantedAuthorities.add(new SimpleGrantedAuthority(r)); });

        Claims claims = Jwts.claims().setSubject(userId+"");
        claims.put("userId", userId);
        claims.put("authorities", grantedAuthorities
                .stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList()));

        return Jwts.builder()
                .setId(UUID.randomUUID().toString())
                .setSubject(userId+"")
                .setClaims(claims)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + TOKEN_LIFESPAN))
                .signWith(key)
                .compact();
    }


}
