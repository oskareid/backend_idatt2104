package no.ntnu.idatt2105.qs3.QS3Backend.model;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "USERACCOUNT")
public class UserAccount {

    @Id
    private int user_id;
    private String first_name;
    private String surname;
    private String email;

    public UserAccount() {}

    public UserAccount(int user_id, String first_name, String surname, String email) {
        this.user_id = user_id;
        this.first_name = first_name;
        this.surname = surname;
        this.email = email;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
