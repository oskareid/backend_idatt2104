package no.ntnu.idatt2105.qs3.QS3Backend.controller.requests;

import javax.servlet.http.HttpServletResponse;
import java.security.Principal;

/**
 * Specifies which items are requires for a SubjectQueuePositions post-request.
 */
public class PostSubjectQueueRequest {

    /**
     * Queue-type ("Approval", or "Help")
     */
    private String queue_type;

    /**
     * Assignment number, -1 if no assignment is specified for the queue position.
     */
    private int assignment_number;

    public PostSubjectQueueRequest() {
    }


    public PostSubjectQueueRequest(String queue_type, int assignment_number) {
        this.queue_type = queue_type;
        this.assignment_number = assignment_number;
    }

    public String getQueue_type() {
        return queue_type;
    }

    public void setQueue_type(String queue_type) {
        this.queue_type = queue_type;
    }

    public int getAssignment_number() {
        return assignment_number;
    }

    public void setAssignment_number(int assignment_number) {
        this.assignment_number = assignment_number;
    }
}
