package no.ntnu.idatt2105.qs3.QS3Backend.service;

import no.ntnu.idatt2105.qs3.QS3Backend.model.Subject;
import no.ntnu.idatt2105.qs3.QS3Backend.model.key.SubjectKey;
import no.ntnu.idatt2105.qs3.QS3Backend.repository.SubjectRepository;
import no.ntnu.idatt2105.qs3.QS3Backend.utilities.ListUtilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SubjectService {

    @Autowired
    private SubjectRepository subjectRepository;

    /**
     * Returns a list of all subjects from the current data foundation
     *
     * @return List of all subjects
     */
    @Transactional(isolation = Isolation.READ_COMMITTED, readOnly = true)
    public List<Subject> getAllSubjects() {
        return ListUtilities.getListFromIterable(
                subjectRepository.findAll()
        );
    }

    @Transactional(isolation = Isolation.READ_COMMITTED, readOnly = true)
    public Subject getSubjectFromKey(SubjectKey key) {
        return subjectRepository.findById(key).orElse(null);
    }


}
