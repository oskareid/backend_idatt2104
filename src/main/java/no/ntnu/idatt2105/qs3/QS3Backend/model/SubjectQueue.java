package no.ntnu.idatt2105.qs3.QS3Backend.model;

import no.ntnu.idatt2105.qs3.QS3Backend.model.key.SubjectKey;

import javax.persistence.*;
import java.util.Objects;

/**
 * Describes the queue of a Subject.
 * Students that are placed in queue is described by {@link SubjectQueuePosition}.
 */
@IdClass(SubjectKey.class)
@Entity
@Table(name="SUBJECTQUEUE")
public class SubjectQueue {

    /**
     * Subject code of the parent subject.
     */
    @Id
    private String subject_code;

    /**
     * Semester of the parent subject
     */
    @Id
    private String semester;

    /**
     * The parent {@link Subject}.
     */
    @OneToOne()
    @JoinColumns({
            @JoinColumn(name="subject_code", updatable = false, insertable = false),
            @JoinColumn(name="semester", updatable = false, insertable = false)
    })
    private Subject subject;

    /**
     * Describes if the queue is active. Allows students to enter queue if it is.
     */
    private boolean active;

    /**
     * Describes if the queue is archived.
     */
    private boolean archived;

    public SubjectQueue() {}

    public SubjectQueue(String subject_code, String semester, Subject subject, boolean active, boolean archived) {
        this.subject_code = subject_code;
        this.semester = semester;
        this.subject = subject;
        this.active = active;
        this.archived = archived;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public String getSubject_code() {
        return subject_code;
    }

    public String getSemester() {
        return semester;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SubjectQueue that = (SubjectQueue) o;

        if (active != that.active) return false;
        if (archived != that.archived) return false;
        if (!Objects.equals(subject_code, that.subject_code)) return false;
        if (!Objects.equals(semester, that.semester)) return false;
        return Objects.equals(subject, that.subject);
    }

}
