package no.ntnu.idatt2105.qs3.QS3Backend.model;

import javax.persistence.Entity;

/**
 * Describes a UserAccount-child which is a Teacher
 */
@Entity
public class Teacher extends UserAccount {

    public Teacher() {
    }

}
