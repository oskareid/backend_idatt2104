package no.ntnu.idatt2105.qs3.QS3Backend.repository;

import no.ntnu.idatt2105.qs3.QS3Backend.model.Subject;
import no.ntnu.idatt2105.qs3.QS3Backend.model.key.SubjectKey;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SubjectRepository extends CrudRepository<Subject, SubjectKey> {

    @Override
    Iterable<Subject> findAll();

    @Override
    Optional<Subject> findById(SubjectKey subjectKey);
}
