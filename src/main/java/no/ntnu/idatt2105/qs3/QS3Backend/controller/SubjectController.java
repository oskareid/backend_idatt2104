package no.ntnu.idatt2105.qs3.QS3Backend.controller;

import no.ntnu.idatt2105.qs3.QS3Backend.model.Subject;
import no.ntnu.idatt2105.qs3.QS3Backend.model.SubjectQueue;
import no.ntnu.idatt2105.qs3.QS3Backend.model.key.SubjectKey;
import no.ntnu.idatt2105.qs3.QS3Backend.security.SecurityConstants;
import no.ntnu.idatt2105.qs3.QS3Backend.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping(value = "/subjects")
@EnableAutoConfiguration
@CrossOrigin
public class SubjectController {

    @Autowired
    private SubjectService subjectService;

    /**
     * Get all subjects available on server.
     *
     * @return All available subjects
     */
    @GetMapping()
    @Secured({SecurityConstants.ROLE_STUDENT, SecurityConstants.ROLE_STUDENT_ASSISTANT})
    public List<Subject> getAllSubjects() {
        return subjectService.getAllSubjects();
    }

    /**
     * Gets information about subject from the unique subject-key
     *
     * @param subject_code To identify subject
     * @param semester To identify subject
     * @return Subject associated with provided key
     */
    @GetMapping("/{subject_code}/{semester}")
    @Secured({SecurityConstants.ROLE_STUDENT, SecurityConstants.ROLE_STUDENT_ASSISTANT})
    public Subject getSubjectFromSubjectKey(
            @PathVariable("subject_code") final String subject_code,
            @PathVariable("semester") final String semester,
            HttpServletResponse response
    ) {
        Subject s = subjectService.getSubjectFromKey(new SubjectKey(subject_code, semester));

        if(s == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return null;
        } else {
            return s;
        }
    }



}
