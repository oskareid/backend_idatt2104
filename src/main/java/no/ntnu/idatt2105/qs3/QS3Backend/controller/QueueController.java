package no.ntnu.idatt2105.qs3.QS3Backend.controller;

import no.ntnu.idatt2105.qs3.QS3Backend.controller.requests.ChangeQueueStatusRequest;
import no.ntnu.idatt2105.qs3.QS3Backend.controller.response.GenericResponse;
import no.ntnu.idatt2105.qs3.QS3Backend.model.SubjectQueuePosition;
import no.ntnu.idatt2105.qs3.QS3Backend.model.SubjectQueue;
import no.ntnu.idatt2105.qs3.QS3Backend.model.key.SubjectKey;
import no.ntnu.idatt2105.qs3.QS3Backend.controller.requests.PostSubjectQueueRequest;
import no.ntnu.idatt2105.qs3.QS3Backend.model.key.SubjectQueuePositionKey;
import no.ntnu.idatt2105.qs3.QS3Backend.security.SecurityConstants;
import no.ntnu.idatt2105.qs3.QS3Backend.service.SubjectQueueService;
import no.ntnu.idatt2105.qs3.QS3Backend.utilities.PrincipalUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping(value = "/subjects/queues")
@EnableAutoConfiguration
@CrossOrigin()
public class QueueController {

    @Autowired
    private SubjectQueueService queueService;

    /**
     * Get all Subject Queues available in database.
     *
     * @return All subject-queues available.
     */
    @GetMapping("")
    public List<SubjectQueue> getAllSubjectQueues() {
        return queueService.getAllSubjectQueues();
    }

    @PostMapping("/{subject_code}/{semester}/status")
    @Secured({SecurityConstants.ROLE_STUDENT_ASSISTANT})
    public GenericResponse postSubjectQueue(
            @RequestBody final ChangeQueueStatusRequest request,
            @PathVariable("subject_code") final String subject_code,
            @PathVariable("semester") final String semester,
            Principal principal
    ) {
        SubjectKey key = new SubjectKey();
        key.setSemester(semester);
        key.setSubject_code(subject_code);
        return new GenericResponse(
                queueService.updateSubjectQueueStatus(key, request.isActive())
        );
    }

    /**
     * Request a list of all active queue positions for the given subject.
     *
     * @param subject_code Subject code for the queue
     * @param semester Semester for the queue (Format {V,H}YY (e.g V22 (Semester Spring 2022))
     * @return List of subject queue postions for the given subject-queue
     */
    @GetMapping("/{subject_code}/{semester}/positions")
    @Secured({SecurityConstants.ROLE_STUDENT, SecurityConstants.ROLE_STUDENT_ASSISTANT})
    public List<SubjectQueuePosition> getQueuePositions(
            @PathVariable("subject_code") final String subject_code,
            @PathVariable("semester") final String semester,
            Principal principal
    ) {
        return queueService.getQueuePositionsBySubjectKey(new SubjectKey(subject_code, semester));
    }

    /**
     * Post a new Queue-position.
     *
     * @param subject_code Subject code for the queue
     * @param semester Semester for the queue (Format {V,H}YY (e.g V22 (Semester Spring 2022))
     * @return List of subject queue postions for the given subject-queue
     */
    @PostMapping("/{subject_code}/{semester}/positions")
    @Secured(SecurityConstants.ROLE_STUDENT)
    @CrossOrigin(origins = "http://localhost:8080")
    public SubjectQueuePosition postQueuePosition(
            @PathVariable("subject_code") final String subject_code,
            @PathVariable("semester") final String semester,
            @RequestBody() final PostSubjectQueueRequest positionRequest,
            Principal principal,
            HttpServletResponse response
    ) {
        if(principal == null) {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return null;
        }

        SubjectQueuePosition position = new SubjectQueuePosition();

        position.setStudent_user_id(PrincipalUtil.getUserId(principal));
        position.setQueue_type(positionRequest.getQueue_type());
        position.setAssignment_number(positionRequest.getAssignment_number());
        position.setSemester(semester);
        position.setSubject_code(subject_code);
        position.setActive_student_assistant_id(-1);

        return queueService.postQueuePosition(position);
    }

    /**
     * Post a new Queue-position or update an existing one.
     *
     * @return List of subject queue postions for the given subject-queue
     */
    @PostMapping("/positions")
    @Secured({SecurityConstants.ROLE_STUDENT_ASSISTANT})
    public SubjectQueuePosition updateQueuePosition(
            @RequestBody() final SubjectQueuePosition queuePosition
    ) {
        return queueService.postQueuePosition(queuePosition);
    }

     /**
     * Deletes the associated QueuePosition
     *
     * @param subject_code Subject code to identify subject for queueposition
     * @param semester To identify subject for queue-position
     * @param student_id To identify student-id
     * @param response
     * @return
     */
    @DeleteMapping("/{subject_code}/{semester}/positions/{student_id}")
    @Secured({SecurityConstants.ROLE_STUDENT_ASSISTANT})
    public boolean deleteQueuePosition(
            @PathVariable("subject_code") final String subject_code,
            @PathVariable("semester") final String semester,
            @PathVariable("student_id") final int student_id,
            HttpServletResponse response
    ) {
        SubjectQueuePositionKey key = new SubjectQueuePositionKey();
        key.setSemester(semester);
        key.setStudent_user_id(student_id);
        key.setSubject_code(subject_code);
        return queueService.deleteQueuePosition(key);
    }


}
