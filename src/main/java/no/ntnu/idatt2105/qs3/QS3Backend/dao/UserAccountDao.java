package no.ntnu.idatt2105.qs3.QS3Backend.dao;

import no.ntnu.idatt2105.qs3.QS3Backend.exception.UnknownCredentialsException;
import no.ntnu.idatt2105.qs3.QS3Backend.model.UserAccount;
import org.springframework.stereotype.Repository;

@Repository
public interface UserAccountDao {

    /**
     * Attempts to verify user-credentials with data-layer. Returns null if no user with provided credentials was found.
     *
     * @param email E-Mail of user
     * @param password Password of user-account
     * @return UserAccount-object of authenticated user, null if authentication failed.
     */
    UserAccount authenticateUser(final String email, final String password) throws UnknownCredentialsException;

    UserAccount readFromEmail(final String email);

    UserAccount readFromId(final int userId);


}
