package no.ntnu.idatt2105.qs3.QS3Backend.model.key;

import java.io.Serializable;
import java.util.Objects;

/**
 * Primary Key structure for {@link no.ntnu.idatt2105.qs3.QS3Backend.model.Assignment}
 */
public class AssignmentKey implements Serializable {

    private String subject_code;
    private String semester;
    private int assignment_number;

    public AssignmentKey() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AssignmentKey that = (AssignmentKey) o;

        if (assignment_number != that.assignment_number) return false;
        if (!Objects.equals(subject_code, that.subject_code)) return false;
        return Objects.equals(semester, that.semester);
    }

    @Override
    public int hashCode() {
        int result = subject_code != null ? subject_code.hashCode() : 0;
        result = 31 * result + (semester != null ? semester.hashCode() : 0);
        result = 31 * result + assignment_number;
        return result;
    }
}
