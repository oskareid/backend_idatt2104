package no.ntnu.idatt2105.qs3.QS3Backend.service;

import no.ntnu.idatt2105.qs3.QS3Backend.dao.impl.UserAccountDaoImpl;
import no.ntnu.idatt2105.qs3.QS3Backend.exception.UnknownCredentialsException;
import no.ntnu.idatt2105.qs3.QS3Backend.model.UserAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserAuthenticationService {

    @Autowired
    private UserAccountDaoImpl userAccountDao;

    @Autowired
    private TokenService tokenService;

    @Autowired
    public UserAuthenticationService() {}

    /**
     * Attempts to authenticate the provided credentials to the UserBase
     *
     * @param username Username of the user to be authenticated, normally the users-email address.
     * @param password Password of the user to be authenticated.
     * @return The authenticated UserAccount object, returns null if the database was unable to find authenticated user.
     */
    @Transactional(isolation = Isolation.READ_COMMITTED, readOnly = true)
    public UserAccount authenticateUser(String username, String password) {
        try {
            return userAccountDao.authenticateUser(username, password);
        } catch (UnknownCredentialsException e) {
            return null;
        }
    }

    public String generateTokenForUserId(int userId) {
        List<String> roles = userAccountDao.getRolesForUserId(userId);
        return tokenService.generateToken(userId, roles);
    }

}
