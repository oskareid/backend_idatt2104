package no.ntnu.idatt2105.qs3.QS3Backend.controller;

import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.ntnu.idatt2105.qs3.QS3Backend.model.UserAccount;
import no.ntnu.idatt2105.qs3.QS3Backend.service.TokenService;
import no.ntnu.idatt2105.qs3.QS3Backend.service.UserAuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping(value = "/token")
@EnableAutoConfiguration
@CrossOrigin
public class TokenController {

    @Autowired
    private TokenService tokenService;

    @Autowired
    private UserAuthenticationService authenticationService;

    /**
     * Retrieves a new token from the authentication service providing the roles, and user-access privileges of the provided username.
     *
     * @param username Username to login with, should be an email
     * @param password Password of the provided user.
     * @return Returns token as string if authentication was successful.
     */
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Authentication successful", content = { @Content(mediaType = "text")}),
            @ApiResponse(responseCode = "401", description = "Username or password was incorrect", content = { @Content(mediaType = "text")})
    })
    @PostMapping(value="")
    public String generateToken(
            @RequestParam("username") final String username,
            @RequestParam("password") final String password,
            HttpServletResponse response
    ) {
        final UserAccount AUTHENTICATED_USER = authenticationService.authenticateUser(username, password);
        final boolean USER_AUTHENTICATED = (AUTHENTICATED_USER != null);

        if(USER_AUTHENTICATED) {
            response.setStatus(HttpServletResponse.SC_CREATED);
            return authenticationService.generateTokenForUserId(AUTHENTICATED_USER.getUser_id());
        } else {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return "Unknown credentials";
        }
    }
}
