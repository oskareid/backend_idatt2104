package no.ntnu.idatt2105.qs3.QS3Backend.service;

import no.ntnu.idatt2105.qs3.QS3Backend.model.Assignment;
import no.ntnu.idatt2105.qs3.QS3Backend.model.AssignmentStatus;
import no.ntnu.idatt2105.qs3.QS3Backend.model.key.AssignmentStatusKey;
import no.ntnu.idatt2105.qs3.QS3Backend.repository.AssignmentStatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;


import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class AssignmentService {

    @Autowired
    private AssignmentStatusRepository assignmentStatusRepository;

    @Transactional(isolation = Isolation.SERIALIZABLE)
    public List<AssignmentStatus> getAssignmentStatusForStudentId(String subject_code, String semester, int student_user_id) {
        return assignmentStatusRepository.findBySubjectKey(subject_code, semester, student_user_id);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public void approveAssignment(String subjectCode, String semester, int student_user_id, int assignment_number) {
        AssignmentStatusKey key = new AssignmentStatusKey();
        key.setSubject_code(subjectCode);
        key.setStudent_user_id(student_user_id);
        key.setAssignment_number(assignment_number);
        key.setSemester(semester);

        AssignmentStatus a = assignmentStatusRepository.findById(key).orElse(null);
        if(a == null) {
            throw new EntityNotFoundException();
        } else {
            a.setApproved(true);
            assignmentStatusRepository.save(a);
        }
    }
}
