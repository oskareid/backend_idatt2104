package no.ntnu.idatt2105.qs3.QS3Backend.security;

public class SecurityConstants {

    //TODO: Generate this key somehow...
    public static final String TOKEN_KEY = "testsecrettestsecrettestsecrettestsecrettestsecret";

    public static final String HTML_AUTH_HEADER_NAME = "Authorization";
    public static final String HTML_TOKEN_HEADER_START_WITH = "Bearer ";

    public static final int DEFAULT_TOKEN_LIFESPAN = 3600000; //60 minutes

    public static final String ROLE_STUDENT = "ROLE_STUDENT";
    public static final String ROLE_STUDENT_ASSISTANT = "ROLE_STUDENT_ASSISTANT";
    public static final String ROLE_TEACHER = "ROLE_TEACHER";
}
