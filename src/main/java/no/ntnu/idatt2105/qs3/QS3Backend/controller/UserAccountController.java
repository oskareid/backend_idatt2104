package no.ntnu.idatt2105.qs3.QS3Backend.controller;

import no.ntnu.idatt2105.qs3.QS3Backend.dao.impl.UserAccountDaoImpl;
import no.ntnu.idatt2105.qs3.QS3Backend.model.UserAccount;
import no.ntnu.idatt2105.qs3.QS3Backend.security.SecurityConstants;
import no.ntnu.idatt2105.qs3.QS3Backend.utilities.PrincipalUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping(value = "/user")
@EnableAutoConfiguration
@CrossOrigin
public class UserAccountController {

    @Autowired
    private UserAccountDaoImpl userDao;

    /**
     * Get the user-details of the provided e-mail address
     *
     * @param email Email registered for the desired user.
     * @return User-details for the desired user.
     */
    @GetMapping(value="/{email}")
    @Secured({SecurityConstants.ROLE_STUDENT, SecurityConstants.ROLE_STUDENT_ASSISTANT})
    public UserAccount getUserByEmail(@PathVariable("email") final String email) {
        return userDao.readFromEmail(email);
    }

    /**
     * Gets the roles of the user associated with the current token-principal.
     *
     * @return List of roles for logged-in user.
     */
    @GetMapping(value="/roles")
    @PreAuthorize("isAuthenticated()")
    @Secured({SecurityConstants.ROLE_STUDENT, SecurityConstants.ROLE_STUDENT_ASSISTANT})
    public List<String> getRolesForCurrentUser(Principal p) {
        int userId = PrincipalUtil.getUserId(p);
        return userDao.getRolesForUserId(userId);
    }

}
