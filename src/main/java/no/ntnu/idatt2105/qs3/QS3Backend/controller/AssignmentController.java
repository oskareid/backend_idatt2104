package no.ntnu.idatt2105.qs3.QS3Backend.controller;

import no.ntnu.idatt2105.qs3.QS3Backend.controller.requests.ApproveAssignmentRequest;
import no.ntnu.idatt2105.qs3.QS3Backend.controller.requests.GetAssignmentStatusForStudentRequest;
import no.ntnu.idatt2105.qs3.QS3Backend.controller.response.GenericResponse;
import no.ntnu.idatt2105.qs3.QS3Backend.model.AssignmentStatus;
import no.ntnu.idatt2105.qs3.QS3Backend.model.Subject;
import no.ntnu.idatt2105.qs3.QS3Backend.model.key.SubjectKey;
import no.ntnu.idatt2105.qs3.QS3Backend.security.SecurityConstants;
import no.ntnu.idatt2105.qs3.QS3Backend.service.AssignmentService;
import no.ntnu.idatt2105.qs3.QS3Backend.utilities.PrincipalUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping(value = "/subjects/assignments")
@EnableAutoConfiguration
@CrossOrigin
public class AssignmentController {

    @Autowired
    private AssignmentService assignmentService;

    /**
     * To be used by student logged in accounts. Returns all the assignment in given subject status associated with student.
     * @param subject_code Subject code to identify subject
     * @param semester Semester to identify subject
     * @return
     */
    @GetMapping("/{subject_code}/{semester}")
    @Secured({SecurityConstants.ROLE_STUDENT})
    public List<AssignmentStatus> getAssignmentStatusForStudent(
            @PathVariable("subject_code") final String subject_code,
            @PathVariable("semester") final String semester,
            Principal principal
    ) {
        int student_user_id = PrincipalUtil.getUserId(principal);
        return assignmentService.getAssignmentStatusForStudentId(subject_code, semester, student_user_id);
    }


    @PostMapping("/{subject_code}/{semester}")
    @Secured({SecurityConstants.ROLE_STUDENT_ASSISTANT})
    public GenericResponse approveAssignment(
            @PathVariable("subject_code") final String subject_code,
            @PathVariable("semester") final String semester,
            @RequestBody final ApproveAssignmentRequest approveAssignmentRequest,
            HttpServletResponse response
    ) {
        try {
            assignmentService.approveAssignment(subject_code, semester, approveAssignmentRequest.getStudent_id(), approveAssignmentRequest.getAssignment_number());
            return new GenericResponse("Approval completed");
        } catch (Exception e) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return new GenericResponse("Unknwon error occured");
        }
    }

}
