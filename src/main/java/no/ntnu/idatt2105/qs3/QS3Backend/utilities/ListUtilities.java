package no.ntnu.idatt2105.qs3.QS3Backend.utilities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListUtilities {

    public static <T> List<T> getListFromIterable(Iterable<T> iterator) {
        List<T>list = new ArrayList<>();
        iterator.forEach(list::add);
        return list;
    }

    public static <T> Iterable<T> toIterableFromArray(T[] arr) {
        return new ArrayList<>(Arrays.asList(arr));
    }

}
