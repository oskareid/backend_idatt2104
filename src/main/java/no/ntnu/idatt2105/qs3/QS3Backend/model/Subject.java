package no.ntnu.idatt2105.qs3.QS3Backend.model;

import no.ntnu.idatt2105.qs3.QS3Backend.model.key.SubjectKey;

import javax.persistence.*;

/**
 * Describes a subject. A Subject is an instance of {@link SubjectInfo} which can occur only once every semester.
 */
@Entity
@IdClass(SubjectKey.class)
public class Subject {

    /**
     * Subject Code of the subject, used to reference correct {@link SubjectInfo}
     */
    @Id
    private String subject_code;

    /**
     * Semester which the subject is running.
     *
     * Format: {First letter of norwegian semester name}|{Two last digits of year}
     * E.g: V22 {Spring 2022}
     */
    @Id
    private String semester;

    /**
     * The associated {@link SubjectInfo} entity.
     */
    @ManyToOne()
    @JoinColumn(name="subject_code", updatable = false, insertable = false)
    private SubjectInfo subjectInfo;

    public Subject() {}

    public Subject(String subject_code, String semester, SubjectInfo subjectInfo) {
        this.subject_code = subject_code;
        this.semester = semester;
        this.subjectInfo = subjectInfo;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public SubjectInfo getSubjectInfo() {
        return subjectInfo;
    }

    public void setSubjectInfo(SubjectInfo subjectInfo) {
        this.subjectInfo = subjectInfo;
    }
}
