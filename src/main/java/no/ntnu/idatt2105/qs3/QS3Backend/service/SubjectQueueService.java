package no.ntnu.idatt2105.qs3.QS3Backend.service;

import no.ntnu.idatt2105.qs3.QS3Backend.model.SubjectQueuePosition;
import no.ntnu.idatt2105.qs3.QS3Backend.model.SubjectQueue;
import no.ntnu.idatt2105.qs3.QS3Backend.model.key.SubjectKey;
import no.ntnu.idatt2105.qs3.QS3Backend.model.key.SubjectQueuePositionKey;
import no.ntnu.idatt2105.qs3.QS3Backend.repository.QueuePositionRepository;
import no.ntnu.idatt2105.qs3.QS3Backend.repository.SubjectQueueRepository;
import no.ntnu.idatt2105.qs3.QS3Backend.utilities.ListUtilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Queue;

@Service
public class SubjectQueueService {

    @Autowired
    private SubjectQueueRepository subjectQueueRepository;
    @Autowired
    private QueuePositionRepository queuePositionRepository;

    /**
     * Returns a list of all subject queues available on the current data foundation.
     * @return List of all subject queues
     */
    @Transactional(isolation = Isolation.READ_COMMITTED, readOnly = true)
    public List<SubjectQueue> getAllSubjectQueues() {
        return ListUtilities.getListFromIterable(
                subjectQueueRepository.findAll()
        );
    }

    /**
     * Returns a list of all subject queue positions of the given subjectKey.
     *
     * @param subjectKey Subject key for queue to search for queue positions
     * @return List of all subject queue positions in the desired queue.
     */
    @Transactional(isolation = Isolation.READ_COMMITTED, readOnly = true)
    public List<SubjectQueuePosition> getQueuePositionsBySubjectKey(SubjectKey subjectKey) {
        return queuePositionRepository.findBySubjectKey(subjectKey.getSubject_code(), subjectKey.getSemester());
    }

    /**
     * Posts new, or updates queue position to the data foundation.
     *
     * @param position Queue position to be added to data foundation.
     * @return Data-object if it succeeded.
     */
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public SubjectQueuePosition postQueuePosition(SubjectQueuePosition position) {
        return queuePositionRepository.save(position);
    }

    /**
     * Deletes the associated QueuePosition
     *
     * @param positionKey Key to uniquely identify SubjectQueuePosition
     * @return True if the delete was successful, false it an error occured.
     */
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public boolean deleteQueuePosition(SubjectQueuePositionKey positionKey) {
        try {
            queuePositionRepository.deleteById(positionKey);
            return true;
        }catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Verify that the provided userid has the provided role in the provided queue.
     *
     * @param queue Queue to check permissions for
     * @param userId UserId to check permissions for
     * @param role Role to verify that the user has
     * @return True if the permission exist, false if the user does not have permission.
     */
    public boolean verifyMembership(SubjectQueue queue, int userId, String role) {
        //TODO: Fix
        return false;
    }

    /**
     * Change the active-status of a queue.
     *
     * @param key Key to identify the queue
     * @param active True if queue should be active, false if nto.
     * @return
     */
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public String updateSubjectQueueStatus(SubjectKey key, boolean active) {
        SubjectQueue q = subjectQueueRepository.findById(key).orElse(null);
        if(q == null) {
            return "Queue with id " + key + " was not found";
        } else {
            q.setActive(active);
            subjectQueueRepository.save(q);
            return "Queue with id " + key + " was sat active to " + active;
        }
    }
}
