package no.ntnu.idatt2105.qs3.QS3Backend.controller.requests;

public class ApproveAssignmentRequest {

    private int student_id;
    private int assignment_number;

    public ApproveAssignmentRequest(int student_id, int assignment_number) {
        this.student_id = student_id;
        this.assignment_number = assignment_number;
    }

    public ApproveAssignmentRequest() {
    }

    public int getStudent_id() {
        return student_id;
    }

    public void setStudent_id(int student_id) {
        this.student_id = student_id;
    }

    public int getAssignment_number() {
        return assignment_number;
    }

    public void setAssignment_number(int assignment_number) {
        this.assignment_number = assignment_number;
    }
}
