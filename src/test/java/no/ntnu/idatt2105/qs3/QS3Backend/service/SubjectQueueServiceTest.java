package no.ntnu.idatt2105.qs3.QS3Backend.service;

import no.ntnu.idatt2105.qs3.QS3Backend.model.SubjectQueue;
import no.ntnu.idatt2105.qs3.QS3Backend.model.SubjectQueuePosition;
import no.ntnu.idatt2105.qs3.QS3Backend.model.key.SubjectKey;
import no.ntnu.idatt2105.qs3.QS3Backend.repository.QueuePositionRepository;
import no.ntnu.idatt2105.qs3.QS3Backend.repository.SubjectQueueRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("SubjectQueueService Unit Tests")
@ExtendWith(MockitoExtension.class)
class SubjectQueueServiceTest {

    @InjectMocks
    private SubjectQueueService mockedService;

    @Mock
    private SubjectQueueRepository subjectQueueRepository;
    @Mock
    private QueuePositionRepository queuePositionRepository;

    private final SubjectQueue sq1 = new SubjectQueue("IDATT2105", "V22", null, false, false);
    private final SubjectQueue sq2 = new SubjectQueue("IDATT2106", "V23", null, false, false);
    private final SubjectQueue sq3 = new SubjectQueue("IDATT2107", "V24", null, false, false);
    private final ArrayList<SubjectQueue> sqList = new ArrayList<>(Arrays.asList(sq1, sq2, sq3));

    private final SubjectQueuePosition qp1 = new SubjectQueuePosition("IDATT2108", "V21", 35, null, "Approval", "Waiting", 4);
    private final SubjectQueuePosition qp2 = new SubjectQueuePosition("IDATT2108", "V21", 36, null, "Approval", "Waiting", 4);
    private final SubjectQueuePosition qp3 = new SubjectQueuePosition("IDATT2108", "V21", 37, null, "Approval", "Waiting", 4);
    private final List<SubjectQueuePosition> qpList = new ArrayList<>(Arrays.asList(qp1, qp2, qp3));


    @BeforeEach
    public void setUp() {
        Mockito.lenient().when(subjectQueueRepository.findAll()).thenReturn(sqList);

        Mockito.lenient().when(queuePositionRepository.findBySubjectKey("IDATT2108", "V21")).thenReturn(qpList);
        Mockito.lenient().when(queuePositionRepository.findBySubjectKey("IDATT2107", "V24")).thenReturn(null);

        Mockito.lenient().when(queuePositionRepository.save(qp1)).thenReturn(qp1);
        Mockito.lenient().when(queuePositionRepository.save(qp2)).thenReturn(null); //TODO: Implement error handling in this function
    }

    @DisplayName("Get all subject queues")
    @Test
    void getAllSubjectQueues() {
        Iterable<SubjectQueue> queues = mockedService.getAllSubjectQueues();

        assertIterableEquals(sqList, queues);
    }

    @DisplayName("Get subject queue position from subject_key")
    @Test
    void getQueuePositionsBySubjectKey() {
        List<SubjectQueuePosition> queuePositions = mockedService.getQueuePositionsBySubjectKey(new SubjectKey("IDATT2108", "V21"));
        List<SubjectQueuePosition> emptyQueuePositions = mockedService.getQueuePositionsBySubjectKey(new SubjectKey("IDATT2107", "V24"));

        assertIterableEquals(qpList, queuePositions);
        assertIterableEquals(null, emptyQueuePositions);
    }

    @DisplayName("Post a new subject queue")
    @Test
    void postNewQueuePosition() {
        SubjectQueuePosition qp = mockedService.postQueuePosition(qp1);

        assertEquals(qp1, qp);
    }
}
