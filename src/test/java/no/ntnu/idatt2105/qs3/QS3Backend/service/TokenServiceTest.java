package no.ntnu.idatt2105.qs3.QS3Backend.service;

import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Base64;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class TokenServiceTest {

    /**
     * Source of decoding https://www.baeldung.com/java-jwt-token-decode
     */
    @DisplayName("Generate new token")
    @Test
    void generateToken() {

        String token = new TokenService().generateToken(123, List.of("TEST_ROLE", "TEST_ROLE2"));

        String[] chunks = token.split("\\.");
        assertEquals(3, chunks.length);

        Base64.Decoder decoder = Base64.getDecoder();
        String header = new String(decoder.decode(chunks[0]));
        String payload = new String(decoder.decode(chunks[1]));

        assertEquals("{\"alg\":\"HS384\"}", header);
        assertThat(payload, CoreMatchers.containsString("{\"sub\":\"123\",\"userId\":123,\"authorities\":[\"TEST_ROLE\",\"TEST_ROLE2\"]"));
    }


}
