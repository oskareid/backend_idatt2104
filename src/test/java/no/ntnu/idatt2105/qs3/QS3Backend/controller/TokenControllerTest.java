package no.ntnu.idatt2105.qs3.QS3Backend.controller;

import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Base64;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@DisplayName("QueueController Integration Tests")
class TokenControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @DisplayName("Valid login")
    @Test
    void generateTokenWithValidLogin() throws Exception {
        MvcResult res = mockMvc.perform(post("/token").
                contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE).
                param("username", "krisvaa@stud.ntnu.no").
                param("password", "password")).
                andExpect(status().is(201)).
                andDo(print()).andReturn();

        String token = res.getResponse().getContentAsString();

        String[] chunks = token.split("\\.");
        assertEquals(3, chunks.length);

        Base64.Decoder decoder = Base64.getDecoder();
        String header = new String(decoder.decode(chunks[0]));
        String payload = new String(decoder.decode(chunks[1]));


        assertEquals("{\"alg\":\"HS384\"}", header);
        assertThat(payload, CoreMatchers.containsString("{\"sub\":\"32\",\"userId\":32,\"authorities\":[\"ROLE_STUDENT\"]"));
    }

    @DisplayName("Invalid login")
    @Test
    void generateTokenWithInvalidLogin() throws Exception {
        MvcResult res = mockMvc.perform(post("/token").
                        contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE).
                        param("username", "krisvaa@stud.ntnu.no").
                        param("password", "p@assw0rd")). //Wrong passwoird
                andExpect(status().is(401)).
                andDo(print()).andReturn();

        String response = res.getResponse().getContentAsString();

        assertEquals("Unknown credentials", response);
    }
}
