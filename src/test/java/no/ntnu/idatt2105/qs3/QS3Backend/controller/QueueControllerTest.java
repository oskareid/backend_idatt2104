package no.ntnu.idatt2105.qs3.QS3Backend.controller;

import no.ntnu.idatt2105.qs3.QS3Backend.service.TokenService;
import org.junit.jupiter.api.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@DisplayName("QueueController Integration Tests")
class QueueControllerTest {

    private Logger logger = Logger.getLogger("QueueControllerTest");

    @Autowired
    private MockMvc mockMvc;

    private static String studentToken;

    private final static String TEST_USER_EMAIL = "krisvaa@stud.ntnu.no";
    private final static int STUDENT_TEST_USER_ID = 32;

    @BeforeEach
    void setUp() {
        studentToken = new TokenService().generateToken(STUDENT_TEST_USER_ID, List.of("ROLE_STUDENT"));
    }

    @AfterEach
    void tearDown() {
    }

    @DisplayName("GET /subjects/queues")
    @Test
    void getAllSubjectQueues() throws Exception {
        MvcResult res = mockMvc.perform(get("/subjects/queues").
                header("authorization", "Bearer " + studentToken)).
                andExpect(status().isOk()).
                andExpect(content().contentType(MediaType.APPLICATION_JSON)).
                andExpect(jsonPath("$", hasSize(greaterThan(1)))).
                andExpect(jsonPath("$[0]", hasKey("subject"))).
                andExpect(jsonPath("$[0]", hasKey("active"))).
                andExpect(jsonPath("$[0]", hasKey("archived"))).
                andDo(print()).
                andReturn();

        logger.info("Result " + res);
    }


    @DisplayName("POST /subjects/queues/{subject_code}/{semester}/positions")
    @Test
    void postQueuePosition() throws Exception {
        String subjectCode = "IDATT2104";
        String semester = "V21";

        String jsonBody = "{\"queue_type\":\"Help\",\"assignment_number\":5}";

        MvcResult res = mockMvc.perform(post("/subjects/queues/"+subjectCode+"/"+semester+"/positions").
                header("authorization", "Bearer " + studentToken).
                content(jsonBody).
                contentType(MediaType.APPLICATION_JSON)).
                andExpect(status().isOk()).
                andExpect(jsonPath("$", hasEntry("subject_code", subjectCode))).
                andExpect(jsonPath("$", hasEntry("semester", semester))).
                andExpect(jsonPath("$", hasEntry("student_user_id", STUDENT_TEST_USER_ID))).
                andDo(print()).
                andExpect(content().contentType(MediaType.APPLICATION_JSON)).andReturn();

        logger.info("Result " + res);
    }
}
