package no.ntnu.idatt2105.qs3.QS3Backend.service;

import no.ntnu.idatt2105.qs3.QS3Backend.dao.impl.UserAccountDaoImpl;
import no.ntnu.idatt2105.qs3.QS3Backend.exception.UnknownCredentialsException;
import no.ntnu.idatt2105.qs3.QS3Backend.model.UserAccount;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Unit test of User Authentication Service")
@ExtendWith(MockitoExtension.class)
class UserAuthenticationServiceTest {

    @InjectMocks
    private UserAuthenticationService mockedService;

    @Mock
    private UserAccountDaoImpl userAccountDao;

    private UserAccount acc;

    @BeforeEach
    void setUp() throws UnknownCredentialsException {
        acc = new UserAccount();
        acc.setEmail("test@testo.no");
        acc.setFirst_name("Test");

        Mockito.lenient().when(userAccountDao.authenticateUser("test@testo.no", "password")).thenReturn(acc);
        Mockito.lenient().when(userAccountDao.authenticateUser("test@testo.no", "password123")).thenThrow(UnknownCredentialsException.class);
    }

    @Test
    void authenticateUser() {
        UserAccount r = mockedService.authenticateUser("test@testo.no", "password");

        assertEquals(acc, r);

        UserAccount nullAcc = mockedService.authenticateUser("test@testo.no", "password123"); //Wrong password
        assertNull(nullAcc);
    }

}
