## QS 3.0 - Backend
Backend for QS 3.0 provides the following features

 - API-Interface for QS-Database
 - Authentication provider (Token based)
 - UserBase
 
__Technologies__
- Maven (Dependency-, and build-system)
- JUnit5, Mockito,   (Unit- and integration-testing)
- Hibernate (Database-Object mapping)
- JPA (Java database API)
- Spring Boot (API-Management)
- Spring Security (API-Authorization manager, JWT-Token generation)
- H2 Database (Database-solution)

## About

### Architecture
The server opens a set of API-Interfaces over the HTTP protocol. These API-Interfaces allow control of
Authentication-requests, Data-Manupilation, and Data-Read. The backend is structured in three layers:


_Client(s)_ -> **API-Interface(Controller)** -> **Service-classes** -> **Database-controller** -

Currently, the database is a H2 database with a static data scheme which is reloaded at each reboot. The scheme is 
located in the resources folder.

### API (Swagger doc)
The API-Documentation can be found at /api/swagger-ui/index.html#/ on a running instance of the server.

### Database
Database-diagram can be found under the docs-folder.

### CI/CD
This repository used Gitlab-CI to run automatic integration- and unit tests.

### Server Configuration
The server runs at port 8085, and has a origin-link /api for all API-Calls. These settings may be changed under application.props.
All requests that are to any other locations than /api/token or /api/h2-ui are blocked unless a valid token is passed along with the request.

### Authentication and Authorization
All requests are authorized trought a JWTTOken filter. This token contains the user-id and the users current roles.
The following roles is recognized by the application:
```json
  "ROLE_STUDENT" //Applied if user is in Student-table in database
  "ROLE_STUDENT_ASSISTANT" //Applied if user is in student-assistant-table in database
  "ROLE_TEACHER" //Appplied if user is in teacher-table in database
```

These roles are dependent on the user's useraccount polymorphic type (Student, StudentAssistant, or Teacher).


#### Passwords
We store passwords in the database. They are stored with the following configuration.
```sql
SHA512(
    RAND_SALT(512) || PASSWORD,
    1000
);
```
The salt, of 512 bits, is stored with the password in the database. Each password is concated with the assicoateed salt.
This value is the fed into a SHA512-Algorithm, with 1000 iterations. The digest is then saved in the database. For future authentication requests, the same procedure is completed,
and the digest is compared to the saved digest. If the digest's match, then the authentication is completed.

## Project Structure

The project is split into the main file-repositories main and test. Main contains the current source-code, while test contains the code for unit tests.

### Config
This folder consists of server-side config, more specifically security-config.

### Model
This package contains the object-representation of tables in the database.

### Service
The service package contains interfaces for each database-repository. 

### dao
The dao package contains logic for communication with the database layer for useraccounts. This will be moved 
to repository in a future release.


### Controller
Controller contains the configuration for all the API-Endpoints offered by the application. This is where user 
authentication beyond the API-Requirement described in earlier section takes place.


### Utilities
This package contains static functions to perform tasks which are often performed.


## Build, run and test
Maven is used to build, run, and test the application. 

**Run tests**
``` bash
mvn test 
```

**Run Server**
``` bash
mvn spring-boot:run 
```

# Known Issues
- There are currently no checks in place to verify if a student assistant has the mentioned
role at assignment accaptence or queue-management. In other words, student-assistants can fully control all queues as they
had the role in the give subject.
- Error handling does not always return the cause. Often the user receives a error 500, with no reason why.
- The secret for generating tokens is a static user-generated string.


# Security
The application protects confidential user-data, student-progress, and passwords and it is therefore crutial to remain a high 
level of security-focus. The following list is an overview of OWASP top 5 most common security holes found in applications 2021. OWASP is a community led 
organization, with the focus to improve security of global software development.
[Read more about OWASP Project Top 10 here](https://owasp.org/www-project-top-ten/)


### 1. Broken access control
Broken access control is a possible security hole for this application. To prevent this from happening, one should focus on 
creating a solid and standarized access system. Integration-tests can also be implemented to check access control.


### 2. Cryptographic Failures
The application uses cryptography to generate tokens, and to save passwords. Failures of cryptography in these  systems could have 
major security impacts. To mitigate these issues one should NEVER change these methods purely for testing purposes, and they should be properly tested in unit- 
or implementation-testing. 

### 3. Injection
QS 3.0 is a target of the injection type SQL-Injection. The solution to Injection-attacts is to sanatize user-input. We currently do not have any 
explicit sanitizing, but JPA sanitizes all input before it is sent to the database. This is important to have in mind in case the application 
would change database-driver some day.

### 4. Insecure Design
Insecure design describes an overall design plan which does not take security into consideration. To mitigate this, it is important as a developer 
to always think about the security aspect of any changes before performing them. This goes for smaller changes, and the overall architecture design.
Abscent conserns for security may easily cause large security holes. 

### 5. Security Misconfiguration
Misconfiguration of security may easily happend if one does not properly educate himself about the configuration possibilities.
It is crucial to properly read documentation before reconfiguring security-parameters in your application.
